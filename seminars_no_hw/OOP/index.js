// class: создание нового класса/модели.
// method: функция внутри класса.
// constructor: метод, который инициирует объект при создании экземпляра класса.
// new: создание объекта через метод конструктора класса.
// extends: используется для определения наследования.
// super: метод, который устанавливает свойства наследования за счет вызова родительского конструктора.
// get: возвращает значение.
// set: переопределяет новое существующее значение.

// 1.создание нового объекта только через new - это помогает привязать контекст.
class User1 {

}

let user1 = new User1();

console.log(user1);

// 2. методы
//символ "_" общепринятое соглашение о приватности
// в JS нет "private", но он есть в TypeScript.

class User2 {
    constructor(name, surname) {
        this._name = name;
        this._surname = surname;
    }

    getFullName() {
        return `Fullname: ${this._name} ${this._surname}`
    }
}

let user2 = new User2('Ivan', 'Ivanov');

console.log(user2.getFullName());

// 3. get set

class User3 {
    constructor(name, surname) {
        this._name = name;
        this._surname = surname;
    }

    getFullName() {
        return `fullname: ${this._name} ${this._surname}`
    }

    get name() {
        return this._name
    }

    get age() {
        return this._age
    }

    set age(value) {
        this._age = value
    }

}

let user3 = new User3('Ivan', 'Ivanov');
user3.age = 30;

console.log(user3.getFullName());
console.log(user3.age);

// 4. extends super

class User4 {
    constructor(name, surname) {
        this._name = name;
        this._surname = surname;
    }

    getFullName() {
        return `fullname: ${this._name} ${this._surname}`
    }

    get name() {
        return this._name
    }

    get age() {
        return this._age
    }

    set age(value) {
        this._age = value
    }

    say() {
        return `Hello, my name is ${this._name} ${this._surname}.`;
    }
}

class Person extends User4 {

    set language(value) {
        this._language = value;
    }

    sayPerson() {
        let say = super.say();
        return `${say} My language ${this._language}. My age ${this.age}`;
    }

}

let userJS = new Person('Petr', 'Sidorov')
let userNode = new Person('Joe', 'Jons')

userJS.language = 'JavaScript'
userJS.age = 25

userNode.language = 'NODEJS'
userNode.age = 35


console.log(userJS.sayPerson());
console.log(userNode.sayPerson());