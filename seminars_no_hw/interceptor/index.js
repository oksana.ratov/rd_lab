// function summa(a, b) {
//     try {
//         if (typeof a !== "number") {
//             throw `Ошибка: ${a} не является числом`;
//         }
//         if (typeof b !== "number") {
//             throw `Ошибка: ${b} не является числом`;
//         }
//         return a + b;
//     } catch (err) {
//         console.error(err);
//         return "Введены некорректные значения";
//     }
// }
// console.log(summa(10, "25"));
// console.log(summa(2, 45));

//========//

// let json = '{"name":"John", "age": 30}';
// let json = '{128}';


// let json = '{"age": 30}';

// try {
//     let user = JSON.parse(json);
//     if (!user.name) {
//         throw new SyntaxError("Данные неполны: нет имени");
//     }
//     console.log(user.name);

// } catch (e) {
//     if (e.name == "SyntaxError") {
//         console.log("JSON Error: " + e.message);
//     } else {
//         throw e;
//     }
//     // }
// } finally {
//     console.log('Код Завершен.');
// }

//===============//

function summa(a, b) {
    try {
        if (typeof a !== "number") {
            throw `Ошибка: ${a} не является числом`;
        }
    } catch (err) {
        return 2
    } finally {
        return 3
    }
}
console.log(summa(10, "25"));